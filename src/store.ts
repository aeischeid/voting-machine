// import { local } from 'svelte-persistent-store';
import { writable } from 'svelte-persistent-store/local.js'; // TS complains, but this is the import that actually works
import type { Writable } from 'svelte/store';
import type { Ballot, Poll } from './main';

// const { writable } = local;

export const poll: Writable<Poll> = writable('poll', {
  name: '',
	description: '',
	items: [],
	password: '',
	limitBallots: false,
	expectedVoters: 0,
	status: 'undefined',
	type: 'RCV',
	winnerCount: 0,
});

export const ballotEntries: Writable<Ballot[]> = writable('ballotEntries', []);

export const theme: Writable<string> = writable('theme', 'nord');
