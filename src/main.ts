import App from './App.svelte';

const app = new App({
	target: document.body,
	props: {}
});

export default app;

export interface PollItem {
	active: boolean;
	label: string;
	id: number;
	description: string;
	rank?: number;
}

export interface RankedPollItem extends PollItem {
	rank: number;
}
export interface Poll {
	name: string;
	description: string;
	items: PollItem[];
	password: string;
	limitBallots: boolean;
	expectedVoters: number;
	status: 'prepared' | 'open' | 'closed' | 'undefined' | 'unlocked';
	type: 'RCV' | 'Star';
	winnerCount: number;
}

export interface Ballot {
	voter: string;
	votes: BallotVote[];
}

export interface BallotVote {
	id: number;
	rank: number;
}
