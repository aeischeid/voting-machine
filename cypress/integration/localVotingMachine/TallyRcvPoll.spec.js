context('Tally ballots for a RCV poll', () => {

	it('should be able to generate a winner report given a RCV poll and set of ballots', () => {

		// Svelte stores will find items in localStorage and load them into app.
		cy.fixture('cartoonPoll').then((pollJson) => {
			cy.visit('/#/tally', {
				onBeforeLoad (win) {
					win.localStorage.setItem('poll', JSON.stringify(pollJson.poll))
					win.localStorage.setItem('ballotEntries', JSON.stringify(pollJson.ballotEntries))
				}
			})
		})
		cy.contains('Count').click()

		// give it just a bit to complete and animate in the report
		cy.wait(1000)
		cy.contains('Winning candidate(s):')
	})
})
