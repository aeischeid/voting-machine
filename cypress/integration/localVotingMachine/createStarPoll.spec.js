context('Create a poll', () => {

	// app currently only supports one poll
	it('should have a functional create poll form', () => {
		cy.visit('/#/build')
		cy.contains('Create A New Poll')

		cy.get('input[name="pollName"]').type('Favorite Cartoon')
		cy.get('input[name="description"]').type('to determine your worth')
		cy.get('input[name="password"]').type('AdventureTime')

		cy.contains('STAR').click()

		cy.get('input#label-0').clear().type('Rick and Morty')
		cy.get('input#description-0').clear().type('Super cynical')

		cy.get('input#label-1').clear().type('Adventure Time')
		cy.get('input#description-1').clear().type('So Math')

		cy.get('input#label-2').clear().type('Simpsons')
		cy.get('input#description-2').clear().type('Doh!')

		cy.contains('Build it').click()
	})
})
