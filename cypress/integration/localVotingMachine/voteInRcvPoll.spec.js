context('Submit a ballot in a RCV poll', () => {

	it('should have a functional ballot form', () => {

		// Svelte stores will find items in localStorage and load them into app.
		cy.fixture('cartoonPoll').then((pollJson) => {
			cy.visit('/#/vote', {
				onBeforeLoad (win) {
					win.localStorage.setItem('poll', JSON.stringify(pollJson.poll))
					win.localStorage.setItem('ballotEntries', "[]")
				}
			})
		})
		cy.contains('Vote for :')

		cy.get('label[aria-label="1 rank"]').eq(0).click()
		cy.get('label[aria-label="2 rank"]').eq(1).click()
		cy.get('label[aria-label="3 rank"]').eq(2).click()

		cy.get('#submitVote').click()
		// there is an animation that needs to complete before the next ballot appears
		cy.wait(1000)

		let ballotEntriesInStorage
		cy.window().then((win) => {
			ballotEntriesInStorage = JSON.parse(win.localStorage.getItem('ballotEntries'))
		}).then(() => {
			// console.log(ballotEntriesInStorage[0].votes)
			// not completely equivilent somehow... :/
			// expect(ballotEntriesInStorage[0].votes).to.equal([{id:45,rank:1},{id:124,rank:2},{id:203,rank:3}])
			expect(ballotEntriesInStorage[0].votes.length).to.equal(3)
			expect(ballotEntriesInStorage[0].votes[0].id).to.equal(45)
			expect(ballotEntriesInStorage[0].votes[0].rank).to.equal(1)
			expect(ballotEntriesInStorage[0].votes[1].id).to.equal(124)
			expect(ballotEntriesInStorage[0].votes[1].rank).to.equal(2)
		})

		cy.get('label[aria-label="1 rank"]').eq(1).click()
		cy.get('label[aria-label="2 rank"]').eq(0).click()
		cy.get('label[aria-label="3 rank"]').eq(2).click()

	})
})
