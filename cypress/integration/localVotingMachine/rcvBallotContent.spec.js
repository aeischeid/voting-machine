context('A ballot in a RCV poll', () => {

	describe('should have poll and candidates information presented', () => {
		beforeEach(() => {
			cy.fixture('cartoonPoll').then((pollJson) => {
				cy.visit('/#/vote', {
					onBeforeLoad (win) {
						win.localStorage.setItem('poll', JSON.stringify(pollJson.poll))
						win.localStorage.setItem('ballotEntries', "[]")
					}
				})
			})
		})

		it('poll name and description', () => {
			cy.contains('Vote for : Favorite Cartoon')
			cy.contains('to determine your worth')
		})

		it('candidates names and descriptions', () => {
			cy.contains('Rick and Morty')
			cy.contains('Super cynical')

			cy.contains('Adventure Time')
			cy.contains('So Math')

			cy.contains('Simpsons')
			cy.contains('Doh!')
		})
	})

	describe('should have ranking options in accordance with number of candidates', () => {

		it('3 candidates lowest rank of 3', () => {

			// Svelte stores will find items in localStorage and load them into app.
			cy.fixture('cartoonPoll').then((pollJson) => {
				cy.visit('/#/vote', {
					onBeforeLoad (win) {
						win.localStorage.setItem('poll', JSON.stringify(pollJson.poll))
						win.localStorage.setItem('ballotEntries', "[]")
					}
				})
			})
			cy.get('label[aria-label="1 rank"]').eq(0).click()
			cy.get('label[aria-label="2 rank"]').eq(1).click()
			cy.get('label[aria-label="3 rank"]').eq(2).click()
			cy.get('label[aria-label="4 rank"]').should('not.exist')
		})

		it('6 candidates lowest rank of 6', () => {

			// Svelte stores will find items in localStorage and load them into app.
			cy.fixture('cartoonPoll').then((pollJson) => {
				cy.visit('/#/vote', {
					onBeforeLoad (win) {
						pollJson.poll.items.push({
							"active":true,
							"label":"Bob's Burgers",
							"id":44,
							"description":"uhhh"
						})
						pollJson.poll.items.push({
							"active":true,
							"label":"Bojack Horseman",
							"id":44,
							"description":"shots of depression"
						})
						pollJson.poll.items.push({
							"active":true,
							"label":"Voltron",
							"id":44,
							"description":"yah"
						})
						win.localStorage.setItem('poll', JSON.stringify(pollJson.poll))
						win.localStorage.setItem('ballotEntries', "[]")
					}
				})
			})
			

			cy.get('label[aria-label="1 rank"]').eq(0).click()
			cy.get('label[aria-label="2 rank"]').eq(1).click()
			cy.get('label[aria-label="3 rank"]').eq(2).click()

			cy.get('label[aria-label="4 rank"]').should('exist')
			cy.get('label[aria-label="5 rank"]').should('exist')
			cy.get('label[aria-label="6 rank"]').should('exist')
			cy.get('label[aria-label="7 rank"]').should('not.exist')
		})
	})
})
