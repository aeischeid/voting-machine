
# A simple Voting Machine

Create a simple single item poll, and have your device run as a voting machine kiosk like thing to gather results from voters and tally the results.

Intended to demonstrate and utilize objectively superior voting mechanisms for fun and serious stuff alike; Chili competitions, favorite movie, or political exit polls maybe.

While practical on its own for small local polls, part of the motivation is to bring awareness to better, more democratic, voting systems like Ranked Choice Voting and Star Voting - both of which have great potential as an integral part of a suite of electoral reforms that could to heal some of what is broken in modern democracies by helping to articulate the voice of the voters more clearly.

### It's a Svelte app

[Svelte](https://svelte.dev)

uses [Svelte-spa-router](https://github.com/ItalyPaleAle/svelte-spa-router)

### To run it locally or help develop
1. clone it
1. have npm 10+
1. `npm install`
1. `npm run dev` 

It is a work in progress, so you know, contributions or comments welcome, but hold off on the criticism, and don't have high expectations for it being fully functional quite yet, much less super polished. It's getting there though.
